<footer>
  <div class="m0a">
    <div>
      <a href="https://www.facebook.com/adampotashapproach"><i class="fab fa-facebook-f"></i></a>
      <a href="https://www.instagram.com/adampotashapproach"><i class="fab fa-instagram"></i></a>
      <a href="https://www.tiktok.com/@adampotashapproach"><i class="fab fa-tiktok"></i></a>
    </div>
  </div>
</footer>

<script type="text/javascript" src="/wp-content/themes/twentytwentyone/js.js"></script>
