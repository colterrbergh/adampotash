<nav class="">
  <!-- co -->
  <div class="container relative">
    <div class="row">
    <div class="col-md-5 col-xs-2" style="background: none;">
      <a href="/"><img class="logo" src="/wp-content/uploads/logo.png" alt=""></a>
    </div>

    <div class="col-md-7 col-xs-10 mob-relative">
      <a class="float-r mt-25 hod" href="javascript:void(0);" onClick="mobileNavToggle()">
        <i class="fa fa-bars tar"></i>
      </a>
  </div>
</div>
</div>
<ul id="mobileNav">
 <li><a href='/'>Home</a></li>
 <li><a href='/weight-loss'>Weight Loss</a></li>
  <li><a href='/blog'>Blog</a></li>
  <li><a class='grey'>Press</a></li>
  <li><a href='/tips-and-recipes'>Tips</a></li>
  <li><a href='/about'>About</a></li>
  <li><a class='grey'>Discovery Call</a></li>
</ul>
</nav>

<script type="text/javascript">

function mobileNavToggle(){
  var mobileNav = document.getElementById("mobileNav");
  if (mobileNav.style.display === "block"){
    mobileNav.style.display = "none";
  }else{
    mobileNav.style.display = "block";
  }
}
// console.log('live');
</script>
