<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php require_once('head.inc.php'); ?>
  </head>
  <!-- <div class="wrapper"> -->
  <body class='tips-and-recipes-page'>
  <!-- </body> -->
    <?php require_once('nav.inc.php'); ?>

    <div id="section2" class="bg-image-food">
      <div class="container">
      </div>

      <div class="slogan">
        <p>Tips for Healthy Living</p>
      </div>
    </div>

    <div id="section3" class="pb50">
      <div class="container">

        <div class="row border-box">
          <div class="list-num">1</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/healthy-diet.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Healthy Diet</p>
            <p class="blurb">You want to eat as close to a natural foods diet as you can. That means a variety of fresh fruits and vegetables, and fewer processed foods. Eat whole grains and high-fiber foods and choose leaner cuts of meat, fish, and poultry. Look to avoid dairy products in your diet or try to have in moderation.</p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num">2</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/exercise.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Exercise</p>
            <p class="blurb">You want to exercise at least 30 minutes a day, three-five days a week. Aerobic exercises (walking, swimming, jogging, bicycling, dancing) are good for women’s health in general and especially for your heart.</p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num">3</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/alcohol.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Alcohol</p>
            <p class="blurb">
              If you drink alcohol, do so in moderation. Most women’s health studies show that women can safely consume one drink a day. Try to stick to clear alcohol - Gin, Vodka or Tequila with club soda.
            </p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num">4</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/stress-management.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Stress Management</p>
            <p class="blurb">
              We all have too much on our plates and want to juggle it all. Stress can have significant health consequences. Try to find a few minutes every day just to relax and get your perspective back again. You also can manage stress with exercise, reading, relaxation techniques, or meditation.
            </p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num">5</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/self-care.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Self Care</p>
            <p class="blurb">
              As a health coach I often talk to my clients about self-care. In our sessions we discuss taking time for yourself in whatever capacity that may be. For some it’s manicures, blowouts, time with friends, a massage, working out, etc.. But self-care can also be how you handle yourself in situations, mental self-care. It helps to produce positive feelings, which improves confidence and self-esteem too.
            </p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num">6</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/balance.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Balance</p>
            <p class="blurb">
              Food is more than what you find on your plate. Healthy relationships, regular physical activity, a fulfilling career and a spiritual practice can fill your soul and satisfy your hunger for life. When primary food is balanced and satiated, your life feeds you.
            </p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num">7</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/meaningful-living.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Meaningful Living</p>
            <p class="blurb">
              Living meaningfully means that we need to accept the fact that there will be bumps, bruises, and perhaps even unhappiness along the way. Creating narratives that help you understand yourself and are filled with purpose, significance, fulfillment, and satisfaction.
            </p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num">8</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/sugar.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Sugar</p>
            <p class="blurb">
              Sugar is found in lots of foods but actually isn't good for us. It's fine to treat yourself in moderation. Cutting out sugar will likely decrease inflammation, boost your energy levels, and improve your ability to focus. Try to avoid foods and beverages that are high in calories and have added sugar. Sugar substitutes as well, they are mostly chemicals. Rule of thumb, if you can’t pronounce it, it’s probably not good for you.
            </p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num">9</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/healthy-snacks.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Healthy Snacks</p>
            <p class="blurb">
              Try some raw vegetables, such as celery, carrots, broccoli, cucumbers, or zucchini with hummus or a dip made from low-fat yogurt. Another great one is fruit with almond butter, or a high quality yogurt line, Siggis. Loaded with probiotics which help with gut balance. Raw nuts are also a great to-go snack. You can throw in purse or keep in car for emergencies.
            </p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num dd">10</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/get-zzzzz.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Get Zzzzz</p>
            <p class="blurb">
              A good night’s sleep is vital to our physical health and emotional well-being. Some benefits of sleep are reduced stress, allows our body to fight back against illness, improves your ability to learn, as well as maintaining weight. Napping is a great way to provide short-term boosts.
            </p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num dd">11</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/seeing-your-doctor.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Seeing Your Doctor </p>
            <p class="blurb">
              It is so important to maintain physical health. We have one body and we need to take care of it. Schedule your annual Pap test to check for cervical cancer. Yearly Mammograms are a must if 40 or above or have a family history of breast cancer. Don't skip your yearly checkup. Your doctor needs to annually assess to ensure your health.
            </p>
          </div>
        </div>

        <div class="row border-box">
          <div class="list-num dd">12</div>
          <div class="col-md-2 vam pl-0"><img src="/wp-content/uploads/being-social.jpeg"></div>
          <div class="col-md-10">
            <p class="title">Being Social</p>
            <p class="blurb">
              Going out to lunch with a friend, seeing a movie with your spouse aren’t just fun activities you do every day. They're also essential for your health. Social engagements can lower stress, better mental health as interacting with others boosts feelings of well-being. New social adventures can be participating in a neighborhood or community group/event, signing you for an adult education class or even attending a religious service. Regardless of how you connect with others, remember that it should be enjoyable to you, this way you will want to often.
            </p>
          </div>
        </div>


      </div>
    </div>

    <div id="newsletter-sign-up" class="newsletter-sign-up">
        <div class="m0a">
          <p class="m0a">Want More Free Tips?</p>
          <input type="text" name="name" value="" placeholder="Name">
          <input type="text" name="email" value="" placeholder="Email">
          <input class="sign-up" type="submit" name="" value="Sign up!">
        </div>
    </div>

    <!-- <div class="eee">
      &nbsp;
    </div> -->

    <div class="healthy-title">
      <div class="container"></div>

      <div class="slogan">
        <p>Tips for Healthy Cooking</p>
      </div>
    </div>

    <div id="section4">
      <div class="container">
        <div class="flex wrap">

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/canned.jpeg" alt="">
              </div>
              <p class="title">Canned and Frozen Are Also Good</p>
              <p class="blurb">
                <ul>
                  <li>Be careful of sodium content in canned items</li>
                  <li>Most frozen are fresh and flash frozen</li>
                  <li>Eat them soon after purchase</li>
                  <li>Steaming are best</li>
                </ul>
              </p>
            </div>

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/organic.jpeg" alt="">
              </div>
              <p class="title">Organic is Better</p>
              <p class="blurb">
                <ul>
                  <li>Keep chemicals off your plate.</li>
                  <li>Protect future generations. Everyone knows this.</li>
                </ul>
              </p>
            </div>

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/local.jpeg" alt="">
              </div>
              <p class="title">Local is Best</p>
              <p class="blurb">
                <ul>
                  <li>Choose farm-to-fork.</li>
                  <li>Support local farmers.</li>
                  <li>Get healthy food into your body.</li>
                </ul>
              </p>
            </div>
          <!-- </div> -->

          <!-- <div class="flex"> -->
            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/timer.jpeg" alt="">
              </div>
              <p class="title">Use a Timer</p>
              <p class="blurb">
                <ul>
                  <li>Time is precious.</li>
                  <li>Get tasks done while you are cooking.</li>
                  <li>Chefs have this built in timer.</li>
                </ul>
              </p>
            </div>

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/simple.jpeg" alt="">
              </div>
              <p class="title">Keep it Simple</p>
              <p class="blurb">
                <ul>
                  <li>Five ingredients or less.</li>
                  <li>Gourmet every so often.</li>
                </ul>
              </p>
            </div>

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/once.jpeg" alt="">
              </div>
              <p class="title">Cook Once, Eat Twice</p>
              <p class="blurb">
                <ul>
                  <li>Cut cooking time/days in half.</li>
                  <li>Spend less time in the kitchen</li>
                  <li>Spend more time doing other things.</li>
                </ul>
              </p>
            </div>
          <!-- </div>

          <div class="flex"> -->

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/vary.jpeg" alt="">
              </div>
              <p class="title">Vary Cooking Styles</p>
              <p class="blurb">
                <ul>
                  <li>Very important.</li>
                  <li>A lot of people I coach get bored.</li>
                  <li>Raw, steam, puree, boil, stir-fry.</li>
                  <li>Soups</li>
                </ul>
              </p>
            </div>

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/herbs.jpeg" alt="">
              </div>
              <p class="title">Herbs & Condiments</p>
              <p class="blurb">
                <ul>
                  <li>Put your herbs and spices on Lazy Susan</li>
                  <li>Hot sauce bar</li>
                  <li>Experiment with different herbs</li>
                </ul>
              </p>
            </div>

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/experiment.jpeg" alt="">
              </div>
              <p class="title">Experiment</p>
              <p class="blurb">
                <ul>
                  <li>Be creative</li>
                  <li>Try new recipes</li>
                </ul>
              </p>
            </div>

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/mistakes.jpeg" alt="">
              </div>
              <p class="title">Mistakes are Okay</p>
              <p class="blurb">
                <ul>
                  <li>Make mistakes</li>
                  <li>Get better every time</li>
                  <li>You can always add but its hard to take away</li>
                </ul>
              </p>
            </div>

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/ask.jpeg" alt="">
              </div>
              <p class="title">Ask Questions</p>
              <p class="blurb">
                <ul>
                  <li>Google.</li>
                  <li>DM, PM, email.</li>
                </ul>
              </p>
            </div>

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/stay-away.jpeg" alt="">
              </div>
              <p class="title">Stay Away From Foods You Don't Like</p>
              <p class="blurb">
                <ul>
                  <li>Avoid marketing to make a decision.</li>
                  <li>Do not be determined to eat okra.</li>
                </ul>
              </p>
            </div>
          <!-- </div>

          <div class="flex"> -->
            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/focus.jpeg" alt="">
              </div>
              <p class="title">Focus On Ways You Enjoy Eating</p>
              <p class="blurb">
                <ul>
                  <li>Alone, with others, chopsticks, times of day.</li>
                </ul>
              </p>
            </div>

            <div class="border-box">
              <div class="m0a">
                <img src="/wp-content/uploads/dont.jpeg" alt="">
              </div>
              <p class="title">Focus On Ways You Enjoy Eating</p>
              <p class="blurb">
                <ul>
                  <li>Have fun with it.</li>
                  <li>Avoid pointing out flaws</li>
                  <li>Take criticism to learn</li>
                </ul>
              </p>
            </div>


            <div class="border-box empty" style="opacity:0;">
              <div class="m0a">
              </div>
              <p class="title"></p>
              <p class="blurb">
                <ul>

                </ul>
              </p>
            </div>
        </div>
      </div>
    </div>


    <div id="section5" class="">
      <div class="healthy-title">
        <div class="container"></div>

        <div class="slogan">
          <p>Cooking Videos</p>
        </div>
      </div>

      <div class="container flex wrap">

        <div class="border-box">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/1bMxW-7uRlc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div class="break">&nbsp;</div>

        <div class="border-box">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/id33MLpX4QA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div class="break">&nbsp;</div>

        <div class="border-box">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/yrdP8gLoog0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div class="break"></div>

        <div class="border-box">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/CUOvCJUOlUg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

      </div>
    </div>

    <div id="section6" class="pb50">
      <div class="healthy-title">
        <div class="container"></div>

        <div class="slogan">
          <p>Healthy & Delicious Recipes</p>
        </div>
      </div>
      <div class="container" class="mt50">
        <div class="flex wrap">

            <div class="border-box m0a">
              <a href="/wp-content/uploads/ChickenCurrywithSpinachandVegetables.pdf" target="_blank">
                <img src="/wp-content/uploads/chicken-curry.jpeg" alt="">
                <p class="title">Chicken Curry with Spinach and Vegetables</p>
              </a>
            </div>

            <div class="border-box m0a">
              <a href="/wp-content/uploads/EasyChickenParmesan.pdf" target="_blank">
                <img src="/wp-content/uploads/chicken-parmesan.jpeg" alt="">
                <p class="title">Easy Chicken Parmesan</p>
              </a>
            </div>

            <div class="border-box m0a">
              <a href="/wp-content/uploads/GreekStyleOmelette.pdf" target="_blank">
                <img src="/wp-content/uploads/greek-style-omelet.jpeg" alt="">
                <p class="title">Greek Style Omelet</p>
              </a>
            </div>

            <div class="border-box m0a">
              <a href="/wp-content/uploads/GrilledAsianFlankSteak.pdf" target="_blank">
                <img src="/wp-content/uploads/grilled-asian-flank-steak.jpeg" alt="">
                <p class="title">Grilled Asian Flank Steak</p>
              </a>
            </div>

            <div class="border-box m0a">
              <a href="/wp-content/uploads/GrilledAsparaguswithTruffleOil.pdf" target="_blank">
                <img src="/wp-content/uploads/grilled-asparagus-with-truffle-oil.jpeg" alt="">
                <p class="title">Grilled Asparagus with Truffle Oil</p>
              </a>
            </div>

            <div class="border-box m0a">
              <a href="/wp-content/uploads/HoneyGlazedSalmon.pdf" target="_blank">
                <img src="/wp-content/uploads/honey-glazed-salmon.jpeg" alt="">
                <p class="title">Honey Glazed Salmon</p>
              </a>
            </div>

            <div class="border-box m0a">
              <a href="/wp-content/uploads/LemonyRoastedFishwithBrusselsSprouts.pdf" target="_blank">
                <img src="/wp-content/uploads/lemony-roasted-fish-with-brussel-sprouts.jpeg" alt="">
                <p class="title">Lemony Roasted Fish with Brussel Sprouts</p>
              </a>
            </div>

            <div class="border-box m0a">
              <a href="/wp-content/uploads/QuinoaStirFrywithVegetables.pdf" target="_blank">
                <img src="/wp-content/uploads/quinoa-stir-fry-with-vegetables.jpeg" alt="">
                <p class="title">Quinoa Stir Fry with Vegetables</p>
              </a>
            </div>

            <div class="border-box m0a">
              <a href="/wp-content/uploads/GrilledChickenwithBroccoliFlorets.pdf" target="_blank">
                <img src="/wp-content/uploads/grilled-chicken-with-broccoli-florets.jpeg" alt="">
                <p class="title">Grilled Chicken with Broccoli Florets</p>
              </a>
            </div>


          </div>
        </div>
      </div>

      <div id="newsletter-sign-up2" class="newsletter-sign-up">
          <div class="m0a">
            <p class="m0a">Sign up for our Newsletter</p>
            <input type="text" name="name" value="" placeholder="Name">
            <input type="text" name="email" value="" placeholder="Email">
            <input class="sign-up" type="submit" name="" value="Sign up!">
          </div>
      </div>

<?php require_once("footerNew.php"); ?>

  <!-- </body> -->

<!-- </div> -->
</html>
