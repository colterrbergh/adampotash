<div class="flex">
  <div class="border-box-responsive flex shadow farm-fresh">
    <div class="lft bg-side mob-bg"></div>
    <div class="rt text-side">

      <p class="name flat-bottom">The Approach <br> One on One Program</p>
      <p class="sub-slogan">Lose up to 25 pounds in 8 weeks</p>
      <p class="quote">
        Feel better than you ever have with the ultimate program in health education - and the last program you will ever need. This 8-week, one-on-one health mentoring and wellness program has exceeded over 2,000+ pounds lost this year. Also available via Skype/Zoom.
      </p>
      <p class="quote">
        Sign up today for our most affordable program. At under $200 you literally have no excuse not to get healthy. The next program is starting soon so don't delay. The time is now!
      </p>

      <p id="seeMoreAction3" class="quote pointer-hover" style="padding-top: 0;">See More</p>

      <div id="seeMoreResult3" class="quote" style="display: none;">
        <span class="included">What's Included</span>
        <ul>
          <li>Complete Health History Overview</li>
          <li>8 Weekly One-on-One Sessions</li>
          <!-- <li>Nourishly Food Logging App</li> -->
          <li>24/7 Text or Phone Accountability</li>
          <li>Elimination Dieting Info</li>
          <li>Education on Blood Type, Sugars and Carbs</li>
          <li>Intermittent Fasting Breakdown</li>
          <li>Pantry Overhaul</li>
          <li>Grocery Store Run</li>
          <li>Halfway Revisit Forms</li>
          <li>6 Free Farm Fresh Meals</li>
        </ul>
      </div>
      <div class="product-button">
        <p>Get Exclusive Coaching</p>
      </div>

    </div>
  </div>
</div>
