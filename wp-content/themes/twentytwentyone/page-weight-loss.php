<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php require_once('head.inc.php'); ?>
  </head>
  <!-- <div class="wrapper"> -->
  <body class='weight-loss-page'>
  </body>
    <?php require_once('nav.inc.php'); ?>

    <div id="section2" class="bg-image">
      <div class="gradient">
        <div class="container">
          <div class="col-12 numbers-dont-lie tac">
            <p class="charcoal">Accountability,</p>
            <p class="charcoal">Support, Simple,</p>
            <!-- <p class="white">Simple</p> -->
            <p class="charcoal">Easy.</p>
            <!-- <p class="yellow numbers">Weight <br> Loss</p> -->
            <!-- <p class="white">Sustainable</p> -->
          </div>
          <!-- <div class="arrow-container">
            <i class="fad fa-angle-double-down"></i>
          </div> -->
          <div class="col-4 blurb numbers-dont-lie">
            <p>If any of these words resonate with you, you’re in the right place. Adam Potash is a certified personal health coach and executive chef, with nearly two decades of experience guiding and mentoring clients in a healthy and sustainable lifestyle in the Boca Raton area. For the first time, the Adam Potash Approach is online and accessible to everyone!</p>
          </div>
        </div>
    </div>

      <!-- <div class="slogan">
        <p><a href="#section4">See what others have to say!</a></p>
      </div> -->
    </div>

    <div id="section3">
      <div class="container">
        <p class="slogan">Weight Loss</p>

        <div class="flex">
          <div class="border-box-responsive flex shadow approach-unlimited">
            <div class="lft bg-side mob-bg"></div>
            <div class="rt text-side">

              <p class="name flat-bottom">The Approach Online <br> Course & Community</p>
              <p class="sub-slogan">Lose up to 16 pounds in 6 weeks</p>
              <p class="quote">
                You too can be a success story and contribute to over 2,000 pounds lost this year. Start anytime with our online course supported by Adam and his staff of amazing Health Coaches. Hear from others that have been through The Approach and how their lives have changed.
              </p>
              <p class="quote">
                Sign up today for our most affordable program. At under $200 you literally have no excuse not to get healthy. The next program is starting soon so don't delay. The time is now!
              </p>

              <p id="seeMoreAction2" class="quote pointer-hover" style="padding-top: 0;">See More</p>

              <div id="seeMoreResult2" style="display: none;">
                <p class="quote">
                  <span class="included">What's Included</span>
                  <ul>
                    <li>6 Intimate Recorded Online Sessions</li>
                    <li>Education on Blood Type, Sugars and Carbs</li>
                    <li>Access to closed Facebook Group</li>
                    <li>Email Questions and Accountability</li>
                    <li>Intermittent Fasting Breakdown</li>
                    <li>Grocery Store Overview</li>
                    <li>Mindful Eating</li>
                  </ul>
                </p>
                <div class="m0a">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/4I7Xis90vPU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
              <div class="product-button">
                <p>Get Started</p>
              </div>
            </div>

          </div>
        </div>

        <div class="flex">
          <div class="border-box-responsive flex shadow starter-packet">
            <div class="lft bg-side mob-bg"></div>
            <div class="rt text-side">

              <p class="name">$17 Starter Packet</p>
              <!-- <p class="sub-slogan">Lost 23 pounds in 10 weeks </p> -->
              <p class="quote">
                Ready to take control? This starter guide is here to assist you in becoming the best version of yourself. The Adam Potash Approach advocates intermittent fasting, using food as fuel, and incorporating a small amount of exercise. With this guide, you receive access to meal plans, shopping lists, workout ideas, and more! Plus, you can connect with like-minded people in our Facebook community. Share tips and connect with others improving their health!
              </p>

              <p id="seeMoreAction1" class="quote pointer-hover" style="padding-top: 0;">See More</p>

              <div id="seeMoreResult1" class="m0a" style="display: none;">
                <iframe width="560" src="https://www.youtube.com/embed/aNiQ7lsL7N4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              <div class="product-button">
                <p>Get healthy now</p>
              </div>

            </div>
          </div>
        </div>

      <?php require_once('healthCoaching.php'); ?>

      </div>



    </div>

    <!-- <div id="section4"> -->
      <!-- <div class="container"> -->
        <!-- <div class="half-bg"> -->
          <!-- <h1>What Our Clients Have To Say</h1> -->
          <!-- <div class="video-box">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/JnP9EtORWBE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            <iframe width="560" height="315" src="https://www.youtube.com/embed/aJuIxN7iC8k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>

          <div class="half-bg right">
            <p>What <br> Our <br> Clients <br> Are <br> Saying</p>
          </div> -->
      <!-- </div> -->
    <!-- </div> -->

<?php require_once("footerNew.php"); ?>

  <!-- </body> -->

<!-- </div> -->
</html>
