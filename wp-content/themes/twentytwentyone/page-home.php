<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php require_once('head.inc.php'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <!-- <div class="wrapper"> -->
  <body class='front-page'>
  </body>
    <?php require_once('nav.inc.php'); ?>

    <div id="section1">
      <div class="gradient shadow">
        <div class="container">
          <div class="col-md-6 hero">
            <!-- <ul>
              <li class="strike">$500</li>
              <li class="strike">$299</li>
              <li class="strike">$99</li>
              <li class='yellow-w-shadow'>19<sup>.95/mo</sup></li>
            </ul> -->
            <p class="slogan">
              <span class="white block">
                <!-- Start Losing</span> <br> Weight Today -->
                Weight Loss
              </span>
              <span class="block">You Need.<span>
              <span class="white block"><!-- With the --> Support</span>
              <span class="block">You Deserve.</span>
            </p>
            <p class="sub-slogan mw"> Weight Loss Course & Support Community</p>
            <div class="m0a">
              <button type="button" name="button" class="join-now m0a">
                <a href="https://checkout.stripe.com/pay/cs_live_a1U2ZqkI0AtOdaaxfeXUbxvNIAc0zlu3p9XixtVmccCVlRt0tqccisMqOW#fidkdWxOYHwnPyd1blppbHNgWnZcZ2MzSEl2dXdiVE9SN3xOdV12dnFrbDU1TjFQVWBxY2onKSd1aWxrbkB9dWp2YGFMYSc%2FJzNqQD1EcDBcSGFrSDNBaDU1NCcpJ3dgY2B3d2B3Jz8nbXFxdXY%2FKipmbWBmbmpwcSt2cXdsdWArZmpoKid4JSUl">
                  Join Now
                </a>
              </button>
            </div>

            <p class="blurb mw">
              The Approach addresses many different aspects of health that go way beyond food intake or exercise.
              We tackle food myths, current diet trends, eating for your blood type, intermittent fasting and so much more.
            </p>
          </div>
        </div>
      </div>
    </div>

    <div id="section2" class="bg-image">
      <div class="container">
        <div class="col-md-4 col-sm-4 numbers-dont-lie">
          <p class="white">Numbers Don't Lie</p>
          <p id="number" class="yellow numbers">12000</p>
          <p class="white">Pounds Lost</p>
        </div>
      </div>
      <div class="slogan">
        <p>If you need to lose weight, you're in the right place</p>
      </div>
    </div>

    <div id="section3" class="before-after">
      <div class="container">
        <p class="slogan">Start Your Journey Today</p>
        <div id="num-slides" class="col-md-12 mob-mb-50">

          <div id="slider1" class="slider">
            <div class="border-box-responsive flex shadow elena">
              <div class="lft bg-side mob-bg"></div>
              <div class="rt text-side">

                <p class="name">Elena, 38</p>
                <p class="sub-slogan">Lost 23 pounds in 10 weeks</p>
                <p class="quote">
                  “The picture on the left I took the day before
                  I started The Approach and the picture on the
                  right I took the day I completed the program.
                  I can’t thank Adam enough. It’s all because of
                  him, his knowledge, and support."
                </p>


              </div>
              <div id="next-button1" class="next-button">
                <img src="/wp-content/uploads/arrow-button.png" alt="">
              </div>
            </div>
          </div>

          <div id="slider2" class="slider" style="display: none;">

            <div class="border-box-responsive flex shadow ari">
              <div class="lft bg-side mob-bg"></div>
              <div class="rt text-side">

                <p class="name">Ari, 46</p>
                <p class="sub-slogan">Lost 46 pounds in 14 weeks</p>
                <p class="quote">
                  "I was working out 5-6 days a week and nothing would budge. It wasn't until I met
                  Adam and started The Approach that my life changed. I literally feel like I got my life back.
                  Thank you Adam."
                </p>


              </div>
              <div id="next-button1" class="next-button">
                <img src="/wp-content/uploads/arrow-button.png" alt="">
              </div>
            </div>

          </div>

          <div id="slider3" class="slider" style="display: none;">

            <div class="border-box-responsive flex shadow rachael">
              <div class="lft bg-side mob-bg"></div>
              <div class="rt text-side">

                <p class="name">Rachael, 42</p>
                <p class="sub-slogan">Lost 18 pounds in 8 weeks</p>
                <p class="quote">
                  "I was such an emotional eater before finding Adam and The Approach.
                  My weight would fluctuate so much. Before The Approach I was just doing everything all wrong.
                  Adam's support changed my life."
                </p>


              </div>
              <div id="next-button1" class="next-button">
                <img src="/wp-content/uploads/arrow-button.png" alt="">
              </div>
            </div>

          </div>

          <div id="slider4" class="slider" style="display: none;">

            <div class="border-box-responsive flex shadow lori">
              <div class="lft bg-side mob-bg"></div>
              <div class="rt text-side">

                <p class="name">Lori, 53</p>
                <p class="sub-slogan">Lost 28 pounds in 15 weeks</p>
                <p class="quote">
                  "I was so skeptical at first. I had tried so many other programs before. My only regret is not
                  starting The Approach sooner. At 53 I have never felt better or looked better in my life."
                </p>


              </div>
              <div id="next-button1" class="next-button">
                <img src="/wp-content/uploads/arrow-button.png" alt="">
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>

    <div id="section4">
        <!-- <div class="container"> -->
          <div class="slogan-container m0a">
            <div class="slogan">
              Start Your Journey <br> Towards A Better You
            </div>
            <p class="sub-slogan">
              We’ll assist and guide you through the
              entire weight loss and health education
              process. Don’t waste another minute
              thinking about making a change.
            </p>
            <p class="sub-slogan">Start your transformation today!</p>
            <button type="button" name="button" class="join-now lite large m0a cap lsn">
              <a style="text-decoration: none;" href="https://checkout.stripe.com/pay/cs_live_a1U2ZqkI0AtOdaaxfeXUbxvNIAc0zlu3p9XixtVmccCVlRt0tqccisMqOW#fidkdWxOYHwnPyd1blppbHNgWnZcZ2MzSEl2dXdiVE9SN3xOdV12dnFrbDU1TjFQVWBxY2onKSd1aWxrbkB9dWp2YGFMYSc%2FJzNqQD1EcDBcSGFrSDNBaDU1NCcpJ3dgY2B3d2B3Jz8nbXFxdXY%2FKipmbWBmbmpwcSt2cXdsdWArZmpoKid4JSUl">
                Join Now
              </a>
            </button>
          </div>
        <!-- </div> -->
    </div>

    <div id="section5" class="shadow">
      <div class="container">
        <div class="slogan">
          So Much More Than <br> Health Coaching or Dieting
        </div>
      </div>
      <div class="yellow-bar-container">
        <div class="flex yellow-bar middle sub-slogan">
          <span>I provide a customized weight loss solution that
          results in a sustainable healthy transformation.</span>
        </div>
      </div>
      <div class="blurb">
        <span class="block">The Approach addresses many different aspects of health that go way beyond food intake or exercise.</span>
        <span class="block">We will be tackling food myths, current diet trends, eating for your blood type, intermittent fasting and so much more.</span>
        <span class="block">Our approach is holistic, informative and hands on. Our passion as health coaches gives us the opportunity to help </span>
        <span class="block">save lives, reignite dreams, and help clients remain motivated to attain their goals. </span>
      </div>
    </div>

    <div id="section6">
      <div class="container flex wrap991 jcsb">
      <div class="vert-card">
        <div class="img-holder one">
          <!-- <img src="" alt=""> -->
        </div>
        <p class="slogan">
          Transformative health advice
        </p>
        <p class="sub-slogan">
          Straight to your inbox. For free.
          Sign up now to receive health
          advice for your daily routine,
          delicious recipes, creative
          workout ideas, and deep
          insights for all-around
          healthy living.
        </p>
          <div class="card-button"><a href="tips-and-recipes/#newsletter-sign-up2">Join Newsletter</a></div>
      </div>

      <div class="vert-card">
        <div class="img-holder two">

        </div>
        <p class="slogan">
          Tips and Tricks for healthy living
        </p>
        <p class="sub-slogan">
          We’ve put together meaning-
          ful health advice, easy-to-
          make recipes, video cooking
          lessons, exhilarating workouts,
          and more exciting, free health
          tips from our team of experts
          to get you started.
        </p>
          <div class="card-button"><a href="tips-and-recipes">30+ Tips & Tricks</a></div>
      </div>

      <div class="vert-card">
        <div class="img-holder three">
          <!-- <img src="" alt=""> -->
        </div>
        <p class="slogan">
          About The Approach
        </p>
        <p class="sub-slogan">
          We all seek to live healthy
          lives, but being overwhelmed
          by stress at work, toxic
          relationships and unhealthy
          cravings leave us seeking
          solutions in the wrong
          places...
        </p>
          <div class="card-button"><a href="products">Learn More</a></div>
      </div>
    </div>

    </div>

    <div id="section7" class="shadow">
      <div class="slogan-container">
        <div class="slogan">
          What do our clients think of the approach?
        </div>
      </div>
      <div class="testimonial-container">
        <div class="quote left">
          <p>“This program is so much more than weight loss. Everyone should take the time and invest in their health.”</p>
          <div class="name">Sherry</div>
        </div>

        <div class="quote right">
          <p>“I can't tell you how much this program has changed my life. I truly can't put into words how
          grateful I am. This is so much more than weight loss.”</p>
          <div class="name">Toby F</div>
        </div>

        <div class="quote left">
          <p>“I have never felt better in my entire life. My only regret is not doing The Approach sooner.”</p>
          <div class="name">Lori</div>
        </div>

      </div>
    </div>

    <div id="section8">
      <div class="flex">
        <div class="fbx">
          <div class="container">
            <?php require_once("healthCoaching.php"); ?>
          </div>
        </div>
      </div>
    </div>

<?php require_once("footerNew.php"); ?>

  <!-- </body> -->

<!-- </div> -->
</html>

<script type="text/javascript">
$(document).ready(function() {
    var target = 13968;
    var number = $('#number').text();

    var interval = setInterval(function() {
       $('#number').text(number);
       if (number >= target) clearInterval(interval);
       number++;
    }, 0);

    let i = 1;
    // Count number of slides
    let numSlides = document.getElementById("num-slides").childElementCount;
    console.log(numSlides);

    $(" .next-button img ").click(function(){
      // Fade out
      $( "#slider"+i ).fadeOut( "slow" );

      // Increment to next slider
      if (i < numSlides ){
        i++;
      }
      else{
        i = 1;
      }

      // Fade in
      $( "#slider"+i ).fadeIn( "slow" );
    })

    // Pretty cool Slider, but how to bring opactiy back?
    // $("#slider"+i).animate({
    //   left: '2050px',
    //   opacity: '0',
    //   height: '200%',
    //   width: '200%'
    // });


});
</script>
