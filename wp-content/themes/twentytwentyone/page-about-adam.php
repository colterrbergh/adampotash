<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php require_once('head.inc.php'); ?>
  </head>
  <!-- <div class="wrapper"> -->
  <body class='about-adam-page'>
  <!-- </body> -->
    <?php require_once('nav.inc.php'); ?>

    <div id="section4">
          <div class="slogan-container m0a">
            <div class="slogan">
              Hi, I'm Adam Potash
            </div>
            <p class="sub-slogan">
              I’ve developed a revolutionary formula that will finally enable you to live a healthy and happy lifestyle.
            </p>

            <button type="button" name="button" class="join-now lite large m0a cap lsn">
              <a style="text-decoration: none;" href="#about-adam">
                Read More
              </a>
            </button>
          </div>
    </div>

    <div id="about-adam">

        <div class="hero">
          <div class="white-line">&nbsp;</div>
          <img src="/wp-content/uploads/about-adam.jpeg" alt="">
        </div>

      <div class="container">
        <div class="flex wrap relative">
          <div class="border-box">
            <p>
              Like many others who struggle with dietary needs, my world was different from birth. As a young child, having extremely high cholesterol meant that I had to constantly be careful and mindful of what I was eating. Under the watchful eye of Jackson Memorial Hospital, I grew up knowing more about eating healthy than many other kids.
            </p>
          </div>

          <div class="border-box">
            <p>My journey toward living healthfully and working with others only truly began after watching my grandmother succumb to illness. During the last years of her life, doctors were quick to prescribe medicines instead of looking into a whole-body, healthy living approach to solve her ailments. Before long, she was taking medications to mask the side effects of other medications. This vicious cycle happens to many people across the nation, younger and older, regardless of the illnesses they are dealing with. I knew right then that I had to help people overcome their struggles as best I could, and I knew exactly how.</p>
          </div>

          <div class="border-box">
            <p>The Approach is a program that offers two core principles to aid not only in weight loss, but prolonged healthfulness. With support and sustainability, people who follow The Approach are sure to find themselves full of natural energy, less stressed, and above all, feeling great from the inside out.</p>
          </div>

          <div class="border-box">
            <p>I’ve always known that food is the key to health. Many weight loss programs focus solely on lowering calories or restricting certain food groups, but these diet fads cannot be sustained long-term due to their extreme limitations. As a student of Johnson and Wales University, I learned about nutrition and the benefits of a full and enriched diet. After my graduation, I went on to the Mandarin Oriental, where I could hone my culinary skills and advance my love of cuisine. Soon, I became an Executive Chef in three different country clubs in the Boca Raton area, and later opened three gourmet cafes of my own. During this time, I continued my education and earned a certification in Health and Nutrition from the Institute of Integrative Nutrition in New York.</p>
          </div>

          <div class="border-box">
            <p>It was after these experiences that I began to cook privately for athletes, sports team owners, and celebrities. Having been an athlete and coach myself, my passion for an active lifestyle, my work in the culinary arts, and my personal struggle with healthy living aided me in developing The Approach.</p>
          </div>

          <div class="border-box">
            <p>I have recorded over 10,000 pounds lost since starting my practice, and clients of mine have lost over 2,300 pounds in the previous year alone. Based on intermittent fasting, The Approach enables people to replace their negative habits with ones that will not only satisfy their palate but help them to feel good from within. The Approach offers a total life support system, sticking with you as you move toward the more healthy, positive life you’ve always wanted.</p>
          </div>

          <div class="border-box button m0a">
            <a class="get-started" href="/products">Get Started!</a>
          </div>

        </div>
      </div>
    </div>

<?php require_once("footerNew.php"); ?>

  <!-- </body> -->

<!-- </div> -->
</html>
